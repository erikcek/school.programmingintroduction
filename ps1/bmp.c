#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "bmp.h"
char toUpper2(char c) {
    if (c >= 97 && c <= 122) {
        return c - 32;
    }
    else {
        return c;
    }
}

char* copyVKey(const char* key, int length) {
    if (key == NULL) {
        return NULL;
    }
    unsigned long klen = strlen(key);
    int index = 0;
    char *r = calloc(length + 1, sizeof(char));
    for (int i=0; i< length; i++) {
        *(r + i) = toUpper2(*(key + index));
        if (index + 1 == klen) {
            index = 0;
        } else {
            index++;
        }
    }
    return r;
}

int isVKeyValid(const char* key) {
    unsigned long l = strlen(key);
    for (int i=0; i< l; i++) {
        char c = *(key + i);
        if (!(c >= 65 && c <= 90) && !(c >= 97 && c <= 122)) {
            return 0;
        }
    }
    return 1;
}

char* reverse(const char* text) {
    if (text == NULL) {
        return NULL;
    }
    unsigned long len = strlen(text);
    char* result = calloc(len + 1, sizeof(char));
    int index = 0;
    for (int i = (int)len-1; i>= 0; i--) {
        *(result + index) = toUpper2(*(text + i));
        index++;
    }
    *(result + index) = '\0';
    return result;
    
}

char* vigenere_encrypt(const char* key, const char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    if (!isVKeyValid(key)) {
        return NULL;
    }
    unsigned long tlen = strlen(text);
    
    char* kk = copyVKey(key, (int)tlen);
    char *r = calloc((int)tlen + 1, sizeof(char));
    
    int index = 0;
    for (int i = 0; i< tlen; i++) {
        if (!(toUpper2(*(text + i)) >= 65 && toUpper2(*(text + i)) >= 65)) {
            *(r + i) = *(text + i);
        }
        else {
            int actCount = (int)toUpper2(*(text + i));
            int upCount = ((int)toUpper2(*(kk + index)) - 65);
            int nextCount = actCount + upCount > 90 ? actCount + upCount - 26 : actCount + upCount;
            *(r + i) = (char)(nextCount);
            index++;
        }
    }
    *(r + tlen) = '\0';
    free(kk);
    return r;
}

char* vigenere_decrypt(const char* key, const char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    if (!isVKeyValid(key)) {
        return NULL;
    }
    unsigned long tlen = strlen(text);
    
    char* kk = copyVKey(key, (int)tlen);
    char *r = calloc((int)tlen + 1, sizeof(char));
    
    int index = 0;
    for (int i = 0; i< tlen; i++) {
        if (!(toUpper2(*(text + i)) >= 65 && toUpper2(*(text + i)) >= 65)) {
            *(r + i) = *(text + i);
        }
        else {
            int actCount = (int)toUpper2(*(text + i));
            int upCount = ((int)toUpper2(*(kk + index)) - 65);
            int nextCount = actCount - upCount < 65 ? actCount - upCount + 26 : actCount - upCount;
            *(r + i) = (char)(nextCount);
            index++;
        }
    }
    *(r + tlen) = '\0';
    free(kk);
    return r;
}

char* firstFour2bin(char c) {
    char *r = calloc(10, sizeof(char));
    int index = 0;
    for (int i = 7; i >= 4; i--) {
        *(r + index) = c & 1 << i ? '1' : '0';
        index++;
    }
    *(r + index) = '\0';
    return r;
}

char* secondFour2bin(char c) {
    char *r = calloc(10, sizeof(char));
    int index = 0;
    for (int i = 3; i >= 0; i--) {
        *(r + index) = c & 1 << i ? '1' : '0';
        index++;
    }
    *(r + index) = '\0';
    return r;
}


void binR(char* c) {
    unsigned long len = strlen(c);
    if (len && len == 4) {
        char* r = calloc(5, sizeof(char));
        strcpy(r, c);
        *c = *(r + 1);
        *(c + 1) = *r;
        *(c + 2) = *(r + 3);
        *(c + 3) = *(r + 2);
        free(r);
    }
    
}

void reduceSpaces2(char* r) {
    if (r != NULL) {
        char* text = calloc(strlen(r) + 1, sizeof(char));
        strcpy(text, r);
        unsigned long len = strlen(text);
        int index = 0;
        for (int i = 0; i< len; i++) {
            char c = toUpper2(*(text + i));
            if (c != ' ') {
                *(r + index) = c;
                index++;
            }
        }
        *(r + index) = '\0';
        free(text);
    }
}


unsigned char* bit_encrypt(const char* text) {
    if (text == NULL) {
        return NULL;
    }
    else {
        unsigned long len = strlen(text);
        int index = 0;
        char *r = calloc(len * 4 + 1, sizeof(char));
        for (int i = 0; i< len; i++) {
            char *res = calloc(10, sizeof(char));;
            *res = '\0';
            char *first = firstFour2bin(text[i]);
            char *second = secondFour2bin(text[i]);
            binR(first);
            
            char c1 = strtol(first, 0, 2);
            char c2 = strtol(second, 0, 2);
            
            char x = (char)c1 ^ c2;
            char *y = secondFour2bin(x);
            
            strcat(res, first);
            strcat(res, y);
            strcat(res, "\0");
            unsigned int data = (unsigned int)strtol(res, NULL, 2);
//            sprintf((r + index), "%x ", data);
            *(r + index) = data;
            index++;
            
            free(res);
            free(first);
            free(second);
            free(y);
        }
        *(r + index) = '\0';
        return (unsigned char*)r;
    }
}

char * s2hs(const unsigned char* text) {
    unsigned long len = strlen((char *)text);
    char *r = calloc(len * 2+1, sizeof(char));
    int index = 0;
    for (int i = 0; i<len; i++) {
        sprintf((r + index), "%x", text[i]);
        index+=2;
    }
    *(r + index) = '\0';
    return r;
}

char* bit_decrypt(const unsigned char* text) {
    if (text == NULL) {
        return NULL;
    }
    else {
        char *myfT = s2hs(text);
        unsigned long len = strlen(myfT);
        char* t = calloc(len + 1, sizeof(char));
        strcat(t, myfT);
        strcat(t, "\0");
        reduceSpaces2(t);
        unsigned long tlen = strlen(t);
        int index = 0;
        char *r = calloc(tlen * 2 + 1, sizeof(char));
        for (int i = 0; i < tlen; i+=2) {
            char *s = calloc(4, sizeof(char));
            *(s + 0) = *(t + i);
            *(s + 1) = *(t + i + 1);
            *(s + 2) = '\0';
            int input = (int)strtol(s, NULL, 16);
            //            printf("%d", input);
            char res[10];
            res[0] = '\0';
            char *first = firstFour2bin(input);
            char *second = secondFour2bin(input);
            //            printf("%s  %s\n", first, second);
            
            char c1 = strtol(first, 0, 2);
            char c2 = strtol(second, 0, 2);
            //
            char x = (char)c1 ^ c2;
            char *secondHalf = secondFour2bin(x);
            //            printf("%s\n", secondHalf);
            
            //            printf("%s\n", first);
            binR(first);
            //            printf("%s\n", first);
            
            
            strcat(res, first);
            strcat(res, secondHalf);
            strcat(res, "\0");
            
            unsigned int data = (unsigned int)strtol(res, NULL, 2);
            sprintf((r + index), "%c", data);
            //            printf("%c", data);
            index+=1;
            
            free(s);
            free(first);
            free(second);
            free(secondHalf);
        }
        free(t);
        free(myfT);
        *(r + index) = '\0';
        return (char*)r;
    }
    
}

unsigned char* bmp_encrypt(const char* key, const char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    char* r = reverse(text);
    if (r == NULL) {
        free(r);
        return NULL;
    }
    char* v = vigenere_encrypt(key, r);
    if (v == NULL) {
        free(r);
        free(v);
        return NULL;
    }
    
    unsigned char* b = bit_encrypt((const char *)v);
    if (b == NULL) {
        free(r);
        free(v);
        free(b);
        return NULL;
    }
    free(r);
    free(v);
    return b;
}

char* bmp_decrypt(const char* key, const unsigned char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    char* r = bit_decrypt(text);
    if (r == NULL) {
        free(r);
        return NULL;
    }
    char* v = vigenere_decrypt(key, r);
    if (v == NULL) {
        free(r);
        free(v);
        return NULL;
    }
    
    char* b = reverse(v);
    if (b == NULL) {
        free(r);
        free(v);
        free(b);
        return NULL;
    }
    free(r);
    free(v);
    return b;
}

