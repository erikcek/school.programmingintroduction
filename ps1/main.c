//
//  playfair.c
//  Top Secret
//
//  Created by Erik Kandalík on 02/03/2019.
//  Copyright © 2019 Erik Kandalík. All rights reserved.
//

#define ALPHA "ABCDEFGHIJKLMNOPQRSTUVXYZ"

#include <stdlib.h>
#include <stdio.h>
#include "playfair.h"
#include "bmp.h"

int main(int argc, const char * argv[]) {
    char *r = playfair_encrypt("test", "test");
    char *o = playfair_decrypt("test", r);
    unsigned char *a = bmp_encrypt("test", "test");
    char *b = bmp_decrypt("test", a);
    free(r);
    free(o);
    free(a);
    free(b);
    return 0;
    
}
