//
//  playfair.c
//  Top Secret
//
//  Created by Erik Kandalík on 02/03/2019.
//  Copyright © 2019 Erik Kandalík. All rights reserved.
//


#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "playfair.h"

void mak(char* s) {
    for (int i=0; i<25; i++) {
        *(s + i) = ALPHA[i];
    }
}

int getI(char mat[5][5], char c) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (mat[i][j] == c) {
                return i;
            }
        }
    }
    return -1;
}

char toUpper(char c) {
    if (c >= 97 && c <= 122) {
        return c - 32;
    }
    else {
        return c;
    }
}

int getJ(char mat[5][5], char c) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (mat[i][j] == c) {
                return j;
            }
        }
    }
    return -1;
}

void reduceSpaces(char* r) {
    if (r != NULL) {
        char* text = calloc(strlen(r) + 1, sizeof(char));
        strcpy(text, r);
        unsigned long len = strlen(text);
        int index = 0;
        for (int i = 0; i< len; i++) {
            char c = toUpper(*(text + i));
            if (c != ' ') {
                *(r + index) = c;
                index++;
            }
        }
        *(r + index) = '\0';
        free(text);
    }
}

void formatResult(char* result) {
    if (result != NULL) {
        unsigned long len = strlen(result);
        char *c = calloc(len + 1, sizeof(char));
        strcpy(c, result);
        int index = 0;
        int t = 0;
        for (int i = 0; i < len; i++) {
            if (t == 2) {
                *(result + index) = ' ';
                t = 0;
                index++;
            }
            *(result + index) = *(c + i);
            t++;
            index++;
        }
        *(result + index) = '\0';
        free(c);
    }
}

void reduceW(char* r) {
    char* text = calloc(strlen(r) + 1, sizeof(char));
    strcpy(text, r);
    for (int i = 0; i< strlen(text); i++) {
        if (*(text + i) == 'W' || *(text + i) == 'w') {
            *(r + i) = 'V';
        }
    }
    free(text);
}

void reduce2Same(char* r) {
    char* text = calloc(strlen(r) + 1, sizeof(char));
    strcpy(text, r);
    int index = 0;
    int i = 0;
    while (i < strlen(text) ) {
        if (*(text + i + 1) == *(text + i) && *(text + i) != 'X') {
            *(r + index) = *(text + i);
            index++;
            *(r + index) = 'X';
            index++;
            i++;
        }
        else {
            *(r + index) = *(text + i);
            index++;
            *(r + index) = *(text + i + 1);
            index++;
            i+=2;
        }
    }
    *(r + index) = '\0';
    free(text);
}

int isKeyValid(const char* key) {
    unsigned long len = strlen(key);
    if (len == 0) {
        return 0;
    }
    for (int i = 0; i< len; i++) {
        char c = *(key + i);
        if (!(c >= 65 && c<=90) && !(c >= 97 && c<=122)) {
            return 0;
        }
    }
    return 1;
}

int isTextValid(const char* key) {
    unsigned long len = strlen(key);
    if (len == 0) {
        return 0;
    }
    for (int i = 0; i< len; i++) {
        char c = *(key + i);
        if ((!(c >= 65 && c<=90) && !(c >= 97 && c<=122)) || toUpper(c) == 'W') {
            return 0;
        }
    }
    return 1;
}


void reduceDuplicate(const char* text, char* r) {
    int index = 0;
    unsigned long len = strlen(text);
    for (int i = 0; i< len; i++) {
        unsigned long l = strlen(r);
        int in = 0;
        for (int j = 0; j < l; j++) {
            if (toUpper(*(text + i)) == toUpper(*(r + j))) {
                in = 1;
            }
        }
        if (!in) {
            *(r + index) = toUpper(*(text + i));
            *(r + index + 1) = '\0';
            index++;
        }
    }
}


char* alphabet(const char* key, char *result) {
    unsigned long keylen = strlen(key);
    char s[25];
    int index = 0;
    mak(s);
    for (int i = 0; i < 25; i++) {
        int in = 0;
        for (int j = 0; j < keylen; j++) {
            if (toUpper(*(s + i)) == toUpper(*(key + j))) {
                in = 1;
            }
        }
        if (!in) {
            *(result + index) = *(s + i);
            index++;
        }
    }
    *(result + index) = '\0';
    return result;
}


char* playfair_encrypt(const char* key, const char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    unsigned long klen = strlen(key);
    unsigned long tlen = strlen(text);
    
    char matrix[5][5];
    char al[25];
    int count = 0;
    int index = 0;
    char* k = calloc(klen + 1, sizeof(char));
    *k = '\0';
    reduceDuplicate(key, k);
    reduceW(k);
    reduceSpaces(k);
    if (!isKeyValid(k)) {
        free(k);
        return NULL;
    }
    
    alphabet(k, al);
    unsigned long l = strlen(k);
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (count < l) {
                matrix[i][j] = toUpper(*(k + count));
                count++;
            }
            else {
                matrix[i][j] = al[index];
                index++;
            }
        }
    }
    
    char* result = calloc(tlen * 3 + 1, sizeof(char));
    char* t = calloc(tlen * 3 + 1, sizeof(char));
    strcpy(t, text);
    reduceSpaces(t);
    reduceW(t);
    reduce2Same(t);
    unsigned long tl = strlen(t);
    
    for (int i=0; i< (tl % 2 == 0 ? tl : tl + 1); i+=2) {
        int i1 = getI(matrix, *(t + i));
        int j1 = getJ(matrix, *(t + i));
        int i2 = getI(matrix, i + 1 >= tl ? 'X' : *(t + i + 1));
        int j2 = getJ(matrix, i + 1 >= tl ? 'X' : *(t + i + 1));
        
        if (j1 == j2) {
            *(result + i) = matrix[i1 + 1 > 4 ? i1 + 1 - 5:  i1 + 1][j1];
            *(result + i + 1) = matrix[i2 + 1 > 4 ? i2 + 1 - 5:  i2 + 1][j2];
            
        }
        else if (i1 == i2) {
            *(result + i) = matrix[i1][j1 + 1 > 4 ? j1 + 1 - 5:  j1 + 1];
            *(result + i + 1) = matrix[i2][j2 + 1 > 4 ? j2 + 1 - 5:  j2 + 1];
            
        }
        else {
            *(result + i) = matrix[i1][j2];
            *(result + i + 1) = matrix[i2][j1];
            
        }
    }
    
    free(k);
    free(t);
    formatResult(result);
    return result;
}

char* playfair_decrypt(const char* key, const char* text) {
    if (key == NULL || text == NULL) {
        return NULL;
    }
    
    unsigned long klen = strlen(key);
    unsigned long tlen = strlen(text);
    
    char matrix[5][5];
    char al[25];
    int count = 0;
    int index = 0;
    char* k = calloc(klen + 1, sizeof(char));
    *k = '\0';
    
    
    
    reduceDuplicate(key, k);
    reduceW(k);
    reduceSpaces(k);
    if (!isKeyValid(k)) {
        free(k);
        return NULL;
    }
    
    alphabet(k, al);
    
    char* t = calloc(tlen * 2 + 1, sizeof(char));
    strcpy(t, text);
    reduceSpaces(t);
    
    if (!isTextValid(t)) {
        free(k);
        free(t);
        return NULL;
    }
    
    
    
    unsigned long l = strlen(k);
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (count < l) {
                matrix[i][j] = toUpper(*(k + count));
                count++;
            }
            else {
                matrix[i][j] = al[index];
                index++;
            }
        }
    }
    
    unsigned long len = strlen(t);
    char* result = calloc(tlen * 4 + 1, sizeof(char));
    for (int i=0; i< (len % 2 == 0 ? len : len + 1); i+=2) {
        int i1 = getI(matrix, *(t + i));
        int j1 = getJ(matrix, *(t + i));
        int i2 = getI(matrix, i + 1 >= len ? 'X' : *(t + i + 1));
        int j2 = getJ(matrix, i + 1 >= len ? 'X' : *(t + i + 1));
        
        if (j1 == j2) {
            *(result + i) = matrix[i1 - 1 < 0 ? i1 - 1 + 5:  i1 - 1][j1];
            *(result + i + 1) = matrix[i2 - 1 < 0 ? i2 -1 + 5:  i2 - 1][j2];
            
        }
        else if (i1 == i2) {
            *(result + i) = matrix[i1][j1 - 1 < 0 ? j1 - 1 + 5:  j1 - 1];
            *(result + i + 1) = matrix[i2][j2 - 1 < 0 ? j2 - 1 + 5:  j2 - 1];
            
        }
        else {
            *(result + i) = matrix[i1][j2];
            *(result + i + 1) = matrix[i2][j1];
            
        }
    }
    
    free(k);
    free(t);
    return result;
};
