#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define M_PI 3.14159265358

double v(double r) {
    return pow(r, 3)*M_PI*4/3;
}

double pV(double r, double h) {
    return M_PI*((pow(h, 2)*r) - (pow(h, 3)) / 3 );
}

double partialVolumeLR(double r, double h) {
    return (v(r) - pV(r, h));
}

double partialVolumeMid(double r, double h1, double h2) {
    return (v(r) - pV(r, h1)) - pV(r, h2);
}

double partialVolumeOfhole(double r, double z, double a1, double a2) {
    double res = 0;
    double right = z + r;
    double left = z- r;
    if (z > a1 && z < a2) {
        if (right >= a2 && left <= a1) {
            // (|#|)
            res = partialVolumeMid(r, right - a2, a1 - left);
        }
        else if (right >= a2 && left >= a1) {
            // (#|)
            res = partialVolumeLR(r, right - a2);
        }
        else if (right <= a2 && left <= a1) {
            // (|#)
            res = partialVolumeLR(r, a1 - left);
        }
        else if (right <= a2 && left >= a1) {
            // ()
            res = v(r);
        }
    }
    else {
        if (right <= a2 && right >= a1) {
            // (|#)
            res = partialVolumeLR(r, a1-left);
        }
        
        else if (left >= a1 && left <= a2) {
            // (#|)
            res = partialVolumeLR(r, right - a2);
        }
        else if (left <= a1 && right >= a2) {
            // (|#|)
            res = partialVolumeMid(r, a1 - left, right - a2);
        }
    }
    return res;
}

void feedInputs(int m, double **inputs) {
    for (int i=0; i<m; i++) {
        double *temp = calloc(4, sizeof(double));
        int a = 0, b = 0, c=0, d=0;
        scanf("%d", &a);
        scanf("%d", &b);
        scanf("%d", &c);
        scanf("%d", &d);
        *(temp + 0) = a / 1000.0;
        *(temp + 1) = b / 1000.0;
        *(temp + 2) = c / 1000.0;
        *(temp + 3) = d / 1000.0;
        *(inputs + i) = temp;
    }
}

double allHoles(double **inputs, int m) {
    double r = 0;
    for (int i=0; i<m; i++) {
        double * temp = *(inputs + i);
        r += v(*temp);
    }
    return r;
}

double allHolesIn(double **holes, double a1, double a2, int n) {
    double res = 0;
    for (int i=0; i<n; i++) {
        double *temp = *(holes + i);
        res+= partialVolumeOfhole(*temp, *(temp + 3), a1, a2);
    }
    return res;
}

double aproximate(double targetVolume, double **holes, double a1, double a2, int a, int n) {
    double res = 0;
    double x1 = a1;
    double x2 = a2;
    do {
        double cube = pow(a, 2)*(x2-x1);
        // vypočítaj aktuálny objem vsetkych dier vo vyseku
        double holesInC = allHolesIn(holes, x1, x2, n);
        res = cube - holesInC;
        if (targetVolume - res < 0) {
            x2 -= (targetVolume - res)/(a*a);
        }
        else {
            x2 += (targetVolume - res)/(a*a);
        }
    } while (fabs(targetVolume-res) > 0.00000001);
    return x2;
}

int main(int argc, const char * argv[]) {
    int a = 100;
    int m = 0;
    int n = 0;
    double holes = 0;
    scanf("%d", &m);
    scanf("%d", &n);
    if (n == 1) {
        printf("%.6f\n", (double)a);
    }
    else {
        if (m != 0) {
            double **inputs = calloc(m, sizeof(double *));
            feedInputs(m, inputs);
            holes = allHoles(inputs, m);
            double appender = 0.1;
            double a1 = 0;
            double a2 = a1 + appender;
            for (int k=0; k<n; k++) {
                double temp = a1;
                a1 = aproximate((pow(a, 3) - holes) / n, inputs, a1, a2, a, m);
                printf("%.6f\n", a1 - temp);
                a2 = a1 + appender;
            }
            for (int i=0; i<m; i++) {
                free(*(inputs + i));
            }
            free(inputs);

        }
        if (holes == 0) {
            for (int i=0; i<n; i++) {
                printf("%.6f\n", (float)a/n);
            }
        }
    }
    return 0;
}