#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


void feedArray(int *array, int len) {
    for (int i=0; i<len-1; i++) {
        *(array + i) = 0;
    }
}

void addToBinaryTreeArray(int item, int index, int *array) {
    if (*(array + index) == 0) {
        *(array + index) = item;
    }
    else if (item > *(array + index)) {
        addToBinaryTreeArray(item, 2+2*index, array);
    }
    else if (item < *(array + index)) {
        addToBinaryTreeArray(item, 1+2*index, array);
    }

}

int main(int argc, const char * argv[]) {
    int n = 0;
    int k = 0;
    scanf("%d", &n);
    scanf("%d", &k);
    if (n > 50 || n < 1) {
        return 1;
    }
    if (k > 20 || k < 1) {
        return 1;
    }
    char **array = calloc(n, sizeof(char *));
    
    int len = 2*pow(2.0, k);
    
    for (int i=0; i< n; i++) {
        int *inputs = calloc(k, sizeof(int));
        for (int j = 0; j<k; j++) {
            scanf("%d", (inputs + j));
        }
        int *binaryArray = calloc(len + 1, sizeof(int));
        char *charArray = calloc(len + 1, sizeof(int));
        feedArray(binaryArray, len);
        for (int j = 0; j<k; j++) {
            addToBinaryTreeArray(*(inputs + j), 0, binaryArray);
        }
        for (int j = 0; j<len; j++) {
            if (*(binaryArray + j) !=0) {
                *(charArray + j) = '1';
            }
            else {
                *(charArray + j) = '0';
            }
        }
        *(charArray + len + 1) = '\0';
        free(binaryArray);
        free(inputs);
        *(array + i) = charArray;
    }
    
    char **arrayDif = calloc(n, sizeof(char *));
    *(arrayDif) = *(array);
    int inx = 0;
    for (int i=0; i< n; i++) {
        int isIn = 0;
        for (int j = 0; j <= inx; j++) {
            if ( strcmp(*(arrayDif + j), *(array + i)) == 0) {
                isIn = 1;
            }
        }
        if (!isIn) {
            inx++;
            *(arrayDif + inx) = *(array + i);
        }

    }

    for (int i=0; i< n; i++) {
        free(*(array + i));
    }
    printf("%d\n", inx + 1);
    free(array);
    free(arrayDif);
    return 0;
}
