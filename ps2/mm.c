
#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    char c;
    unsigned int sum = 0;
    c = getc(stdin);
    while (c != '\n') {
        if (c == '\n') {
            break;
        }
        sum += (int)(c - 48);
        c = getc(stdin);
    }
    unsigned int temp = 0;
    while(sum > 9) {
        temp = sum;
        sum = 0;
        while (temp != 0) {
            sum += temp % 10;
            temp = temp / 10;
        }
    }
    printf("%u\n", sum);
    return 0;
}
