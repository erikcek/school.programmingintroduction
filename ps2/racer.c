
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


int main(int argc, const char * argv[]) {
    int n = 0;
    int t = 0;
    double boundary = 9999999;
    double minFactor = 0.0000001;
    scanf("%d", &n);
    scanf("%d", &t);
    
    int **inputs = calloc(n, sizeof(sizeof(int *)));
    
    for (int i=0; i<n; i++) {
        int *line = calloc(2, sizeof(int));
        scanf("%d", (line));
        scanf("%d", (line + 1));
        *(inputs + i) = line;
    }
    
    
    int largest = 0;
    for (int i=0; i<n; i++) {
        int *temp = *(inputs + i);
        //        printf("i= %d %d\n",i, *(temp + 0));
        //        printf("i= %d %d\n",i, *(temp + 1));
        if (*(temp + 1) < largest) {
            largest = *(temp + 1);
        }
    }
    
    int largestPLus = 0;
    for (int i=0; i<n; i++) {
        int *temp = *(inputs + i);
        if (*(temp + 1) > largestPLus) {
            largestPLus = *(temp + 1);
        }
    }
    
    
    long double dif = 0;
    long double k = largest*(-1) + minFactor;
    long double minK, maxK = 0;
    //    minK = largest*(-1) != 0 ? largest*(-1) : -1 * largestPLus;
    minK = boundary * -1;
    maxK = boundary;
    dif = t;
    long double x = 0;
    long double r = 0;
    long double lastX = -1.0125;
    long double lastK = -1.1243;
    do {
        x = 0;
        int isLower = 0;
        k = minK + (maxK - minK) / 2.0;
        //        k = roundf(k * 1000000000000) / 1000000000000;
        for (int i = 0; i<n; i++) {
            int *temp = *(inputs + i);
            if ((*(temp + 1) + k) != 0) {
                r = *(temp) / (long double)(*(temp + 1) + k);
                if (r < 0) {
                    isLower = 1;
                }
                x += r;
            }
            else {
                isLower = 1;
            }
            
        }
        if (x == t) {
            break;
        }
        if (lastX == x) {
            dif = t - roundf(x * 100000000000) / 1000000000000;
        }
        else {
            dif = t - x;
        }
        if (x < 0 || isLower) {
            minK = k;
        }
        else if (x > t) {
            
            minK = k;
        }
        else if (x < t){
            
            maxK = k;
        }
        if (dif < 0) {
            dif *= (-1);
        }
        lastX = x;
        if (round(k * 10000000) / 10000000 == lastK) {
            break;
        }
        lastK = round(k * 10000000) / 10000000;
    } while (dif >= minFactor/ (100 - n));
    
    
    printf("%.6Lf", k);
    for (int i=0; i<n; i++) {
        free(*(inputs + i));
    }
    free(inputs);
    
    return 0;
}


// 1 1 1000 1000
// 1 1000000 1000 0


