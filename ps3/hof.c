#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hof.h"

int compareFunction (const void *p1, const void *p2) {
    return ((struct player *)p2)->score - ((struct player *)p1)->score;
}

int load(struct player list[]) {
    FILE *f = fopen("score","r");
    if (f == NULL) {
        return -1;
    }
    long unsigned int idx = 0;
    char c = (char)fgetc(f);
    struct player ll[100];
    while (c != EOF && idx < 100) {
        fseek(f, -1, SEEK_CUR);
        struct player p;
        fscanf(f, "%s %d", p.name, &(p.score));
        ll[idx] = p;
        idx++;
        c = (char)fgetc(f);
        if (c == '\n'){
            c = (char)fgetc(f);
        }
    }
    fclose(f);
    qsort(ll, idx, sizeof(struct player), compareFunction);
    if (idx >= 9) {
        for (int i =0; i<10; i++) {
            list[i] = ll[i];
        }    }
    else {
        for (int i =0; i<=idx; i++) {
            list[i] = ll[i];
        }

    }
    if (idx > 9) {
        return 10;
    }
    else {
        return (int)idx;
    }
}

bool save(const struct player list[], const int size) {
    FILE *f = fopen("score", "w");
    for (int i=0; i<size; i++) {
        fprintf(f, "%s %d\n", list[i].name, list[i].score);
    }
    fclose(f);
    return true;
}

bool add_player(struct player list[], int* size, const struct player player) {
    bool res = false;
    if (*size <= 0) {
        res = true;
        list[0] = player;
        *size +=1;
    }
    else if (*size >= 1 && *size <= 9) {
        res = true;
        int idx = *size;
        for (int i=0; i<*size; i++) {
            if (player.score >= list[i].score) {
                idx = i;
                break;
            }
        }
    
        for (int i=*size; i>idx; i--) {
            list[i] = list[i-1];
        }
        list[idx] = player;
        *size += 1;
    }
    else {
        if (list[*size-1].score <= player.score) {
            res = true;
            int idx = 0;
            for (int i=0; i<*size; i++) {
                if (player.score >= list[i].score) {
                    idx = i;
                    break;
                }
            }
                for (int i=*size -1; i>idx; i--) {
                    list[i] = list[i-1];
                }
                list[idx] = player;
            }
    }
    return res;
}
