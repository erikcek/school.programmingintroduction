#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "k.h"

void feedRow(int *row, struct game *game, int index) {
    int i = 0;
    for(int j=0; j<SIZE; j++) {
        if (game->board[index][j] != ' ') {
            *(row + i) = game->board[index][j];
            i++;
        }
    }
    for (int x=i; x<SIZE; x++) {
        *(row + x) = ' ';
    }
}

void feedRowReverse(int *row, struct game *game, int index) {
    int i = 0;
    for(int j=SIZE-1; j>=0; j--) {
        if (game->board[index][j] != ' ') {
            *(row + i) = game->board[index][j];
            i++;
        }
    }
    for (int x=i; x<SIZE; x++) {
        *(row + x) = ' ';
    }
}

void feedColumn(int *col, struct game *game, int index) {
    int i = 0;
    for(int j=0; j<SIZE; j++) {
        if (game->board[j][index] != ' ') {
            *(col + i) = game->board[j][index];
            i++;
        }
    }
    for (int x=i; x<SIZE; x++) {
        *(col + x) = ' ';
    }
}

void feedColumnReverse(int *col, struct game *game, int index) {
    int i = 0;
    for(int j=SIZE-1; j>=0; j--) {
        if (game->board[j][index] != ' ') {
            *(col + i) = game->board[j][index];
            i++;
        }
    }
    for (int x=i; x<SIZE; x++) {
        *(col + x) = ' ';
    }
}


int* updateRow(int *row, struct game *game) {
    int index = 0;
    int i = 0;
    int *r = calloc(SIZE, sizeof(int));
    while(i<SIZE-1) {
        if (*(row + i) == *(row + i + 1) && *(row + i) != ' ') {
            *(r + index) = *(row + i) + 1;
            int x = (int)(*(row + i) + 1 - 'A' + 1);
            game->score += (int)pow(2,x);
            index++;
            i+=2;
        }
        else {
            *(r + index) = *(row + i);
            index++;
            i+=1;
        }
    }
    if (i == 4) {
        *(r + index) = ' ';
    }
    else if (*(row + SIZE-1) != ' ') {
        *(r + index) = *(row + i);
        index++;
    }
    for (int x=index; x<SIZE; x++) {
        *(r + x) = ' ';
    }
    return r;
}

void add_random_tile(struct game *game){
    int row, col;
    // find random, but empty tile
    do{
        row = rand() % SIZE;
        col = rand() % SIZE;
    }while(game->board[row][col] != ' ');
    
    // place to the random position 'A' or 'B' tile
    if(rand() % 2 == 0){
        game->board[row][col] = 'A';
    }else{
        game->board[row][col] = 'B';
    }
}

bool is_game_won(const struct game game) {
    for (int i=0; i<SIZE; i++) {
        for (int j=0; j<SIZE; j++) {
            if (game.board[i][j] == 'K') {
                return true;
            }
        }
    }
    return false;
};

bool is_move_possible(const struct game game) {
    for (int i=0; i<SIZE; i++) {
        for (int j=0; j<SIZE; j++) {
            if (game.board[i][j] == ' ') {
                return true;
            }
        }
    }
    
    for (int i=0; i<SIZE; i++) {
        char prev = game.board[i][0];
        for (int j=1; j<SIZE; j++) {
            if (game.board[i][j] == prev) {
                return true;
            }
            prev = game.board[i][j];
        }
    }
    
    for (int j=0; j<SIZE; j++) {
        char prev = game.board[0][j];
        for (int i=1; i<SIZE; i++) {
            if (game.board[i][j] == prev) {
                return true;
            }
            prev = game.board[i][j];
        }
    }
    return false;
}

bool update(struct game *game, int dy, int dx) {
    if (abs(dx) == abs(dy)) {
        return false;
    }
    bool res = false;
    if (dx == -1) {
        for (int i=0; i<SIZE; i++) {
            int *row = calloc(SIZE, sizeof(int));
            feedRow(row, game, i);
            int *transformedRow = updateRow(row, game);
            for (int j=0; j<SIZE; j++) {
                if (game->board[i][j] != (char)*(transformedRow + j)) {
                    res = true;
                }
                game->board[i][j] = (char)*(transformedRow + j);
            }
            free(row);
            free(transformedRow);
        }
    }
    if (dx == 1) {
        for (int i=0; i<SIZE; i++) {
            int *row = calloc(SIZE, sizeof(int));
            feedRowReverse(row, game, i);
            int *transformedRow = updateRow(row, game);
            for (int j=SIZE-1; j>=0; j--) {
                if  (game->board[i][j] != (char)*(transformedRow + SIZE - j - 1)) {
                    res = true;
                }
                game->board[i][j] = (char)*(transformedRow + SIZE - j - 1);
            }
            free(row);
            free(transformedRow);
        }
    }
    if (dy == -1) {
        for (int j=0; j<SIZE; j++) {
            int *col = calloc(SIZE, sizeof(int));
            feedColumn(col, game, j);
            int *transformedCol = updateRow(col, game);
            for (int i=0; i<SIZE; i++) {
                if  (game->board[i][j] != (char)*(transformedCol + i)) {
                    res = true;
                }
                game->board[i][j] = (char)*(transformedCol + i);
            }
            free(col);
            free(transformedCol);
        }
    }
    if (dy == 1) {
        for (int j=0; j<SIZE; j++) {
            int *col = calloc(SIZE, sizeof(int));
            feedColumnReverse(col, game, j);
            int *transformedCol = updateRow(col, game);
            for (int i=SIZE-1; i>=0; i--) {
                if  (game->board[i][j] != (char)*(transformedCol + SIZE - i - 1)) {
                    res = true;
                }
                game->board[i][j] = (char)*(transformedCol + SIZE - i - 1);
            }
            free(col);
            free(transformedCol);
        }
    }

    
    return res;
}
