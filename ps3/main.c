#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "k.h"
#include "hof.h"
#include "ui.h"

//void prg(struct game g) {
//    printf("____________\n");
//    for (int i=0; i<SIZE; i++) {
//        for (int j=0; j<SIZE; j++) {
//            printf("%c ", g.board[i][j]);
//        }
//        printf(" - \n");
//    }
//    printf("____________\n");
//}

int main(int argc, const char * argv[]) {
    struct player p[10];
    struct player a;
    strcpy(a.name, "aaaaaaa");
    a.score = 4500;
    struct game game = {
        .board = {
            {'A', ' ', ' ', ' '},
            {' ', ' ', ' ', ' '},
            {' ', ' ', ' ', 'A'},
            {'B', ' ', ' ', ' '}
        },
        .score = 0
    };
    add_random_tile(&game);
    is_game_won(game);
    is_move_possible(game);
    update(&game, 0, 1);
    int s = load(p);
    add_player(p, &s, a);
    save(p, s);
    render(game);
    
    return 0;
}


