#include "backpack.h"
#include <stdlib.h>
#include <string.h>

struct backpack* create_backpack(const int capacity) {
    struct backpack *nb = calloc(1, sizeof(struct backpack));
    
    nb->capacity = capacity;
    nb->size = 0;
    nb->items = NULL;
    
    return nb;
}

struct backpack* destroy_backpack(struct backpack* backpack) {
    if (backpack != NULL) {
        if (backpack->items != NULL) {
            destroy_containers(backpack->items);
        }
        free(backpack);
    }
    return NULL;
}

bool add_item_to_backpack(struct backpack* backpack, struct item* item) {
    if (backpack == NULL || item == NULL) {
        return false;
    }
    if (backpack->size == backpack->capacity) {
        return false;
    }
    if (item->properties % 2 == 0) {
        return false;
    }
    
    if (backpack->items == NULL) {
        backpack->items = create_container(NULL, ITEM, item);
    }
    else {
        create_container(backpack->items, ITEM, item);
    }
    
    if (backpack->items != NULL) {
        backpack->size += 1;
        return true;
    }
    else{
        return false;
    }
}

struct item* get_item_from_backpack(const struct backpack* backpack, char* name) {
    if (backpack != NULL && backpack->items != NULL) {
        struct container *ci = backpack->items;
        return get_from_container_by_name(ci, name);
    }
    return NULL;
}

void delete_item_from_backpack(struct backpack* backpack, struct item* item) {
    if (backpack != NULL && backpack->items != NULL && item != NULL) {
        struct container *c = backpack->items;
        if (c->item == item) {
            backpack->items = c->next;
            free(c);
            backpack->size -= 1;
        }
        else {
            while (c->next != NULL && c->next->item != item) {
                if (c->next->next == NULL) {
                    break;
                }
                c = c->next;
            }
            if (c->next != NULL && c->next->item == item) {
                struct container *i = c->next;
                c->next = c->next->next;
                free(i);
                backpack->size -= 1;
            }
        }
    }
}
