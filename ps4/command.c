#include "command.h"
#include <stdlib.h>
#include <string.h>



struct command* create_command(char* name, char* description, char* pattern, size_t nmatch) {
    if (name == NULL || description == NULL) {
        return NULL;
    }
    if (strcmp(name, "") == 0 || strcmp(description, "") == 0) {
        return NULL;
    }
    
//    regex_t *regex = calloc(1, sizeof(regex_t));
//    if (pattern != NULL) {
//        if(regcomp(regex, pattern, REG_ICASE | REG_EXTENDED) != 0) {
//            return NULL;
//        }
//    }
//    else {
//        if(regcomp(regex, name, REG_ICASE | REG_EXTENDED) != 0) {
//            return NULL;
//        }
//    }
    
    char *new_name = calloc(strlen(name) + 1, sizeof(char));
    char *new_description = calloc(strlen(description) + 1, sizeof(char));
    strcpy(new_name, name);
    strcpy(new_description, description);
    
    struct command *nc = calloc(1, sizeof(struct command));
    nc->name = new_name;
//    nc->preg = *regex;
    nc->description = new_description;
    nc->nmatch = nmatch;
    
//    if (nmatch > 0) {
//        char **new_groups = calloc(nmatch, sizeof(char *));
//        nc->groups = new_groups;
//    }

    
    if (pattern != NULL) {
        regcomp(&nc->preg, pattern, REG_ICASE | REG_EXTENDED);
    }
//            return NULL;
//        }
//    }
//    else {
//        if(regcomp(&nc->preg, name, REG_ICASE | REG_EXTENDED) != 0) {
//            return NULL;
//        }
//    }
//
    
    return nc;
}



struct command* destroy_command(struct command* command) {
    if (command != NULL) {
        regfree(&command->preg);
        free(command->description);
        free(command->name);
//        free(&command->preg);
        if (command->groups != NULL) {
            for (int i=0; i<command->nmatch; i++) {
                free(command->groups[i]);
            }
            free(command->groups);
        }
        free(command);
    }
    return NULL;
}
