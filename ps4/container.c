#include "container.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>


struct container* create_container(struct container* first, enum container_type type, void* entry) {
    //conctroling input
    if (entry == NULL) {
        return NULL;
    }
    if (first != NULL) {
        if (first->type != type) {
            return NULL;
        }
    }
    //creating struct
    struct container *new = calloc(1, sizeof(struct container));
    new->type = type;
    new->next = NULL;
    if (type == ROOM) {
        new->room = entry;
    }
    if (type == ITEM) {
        new->item = entry;
    }
    if (type == COMMAND) {
        new->command = entry;
    }
    if (type == TEXT) {
        new->text = entry;
    }
    
    if (first == NULL) {
        return new;
    }
    
    // finding last item in connected list
    struct container *current = first;
    while (current->next != NULL) {
        current = current->next;
    }
    
    //creting / returning new container
    current->next = new;
    return new;
}

struct container* destroy_containers(struct container* first) {
    if (first == NULL) {
        return NULL;
    }
    struct container *current = first;
    struct container *next = first->next;

    while(next != NULL) {
        if (current->type == ITEM) {
            destroy_item(current->item);
        }
        if (current->type == COMMAND) {
            destroy_command(current->command);
        }
        if (current->type == ROOM) {
            destroy_room(current->room);
        }
				if (current->type == TEXT) {
						free(current->text);
				}
        free(current);
        current = next;
        next = next->next;
    }
    if (current->type == ITEM) {
        destroy_item(current->item);
    }
    if (current->type == COMMAND) {
        destroy_command(current->command);
    }
    if (current->type == ROOM) {
        destroy_room(current->room);
    }
		if (current->type == TEXT) {
				free(current->text);
		}
    free(current);
    return NULL;
}

void* get_from_container_by_name(struct container *first, const char *name) {
    if (first == NULL) {
        return NULL;
    }
    struct container *c = first;
    if (first->type == ITEM) {
        while (strcasecmp(c->item->name, name) != 0) {
            if (c->next == NULL) {
                return NULL;
            }
            c = c->next;
        }
        return c->item;
    }
    if (first->type == ROOM) {
        while (strcasecmp(c->room->name, name) != 0) {
            if (c->next == NULL) {
                return NULL;
            }
            c = c->next;
        }
        return c->room;
    }
    if (first->type == COMMAND) {
        while (strcasecmp(c->command->name, name) != 0) {
            if (c->next == NULL) {
                return NULL;
            }
            c = c->next;
        }
        return c->command;
    }
    if (first->type == TEXT) {
        while (strcasecmp(c->text, name) != 0) {
            if (c->next == NULL) {
                return NULL;
            }
            c = c->next;
        }
        return c->text;
    }

    return NULL;
}

struct container* remove_container(struct container *first, void *entry) {
    if (first == NULL) {
        return NULL;
    }
    struct container *c = first;
    if (first->type == ITEM) {
        if (c->item == entry) {
						struct container *temp = c->next;
						free(c);
            return temp;
        }
        else {
            while (c->next != NULL && c->next->item != entry) {
                if (c->next->next == NULL ) {
                    break;
                }
                c = c->next;
            }
            if (c->next != NULL && c->next->item == entry) {
								struct container *temp = c->next;
                c->next = c->next->next;
								free(temp);
            }
            return first;
        }
    }
    if (first->type == COMMAND) {
        if (c->command == entry) {
						struct container *temp = c->next;
            free(c);
            return temp;
        }
        else {
            while (c->next != NULL && c->next->command != entry) {
                if (c->next->next == NULL ) {
                    break;
                }
                c = c->next;
            }
            if (c->next != NULL && c->next->command == entry) {
								struct container *temp = c->next;
                c->next = c->next->next;
								free(temp);
            }
            return first;

        }
    }
    if (first->type == ROOM) {
        if (c->room == entry) {
						struct container *temp = c->next;
            free(c);
            return temp;
        }
        else {
            while (c->next != NULL && c->next->room != entry) {
                if (c->next->next == NULL) {
                    break;
                }

                c = c->next;
            }
            if (c->next != NULL && c->next->room == entry) {
								struct container *temp = c->next;
                c->next = c->next->next;
								free(temp);
            }
            return first;
        }
    }
    if (first->type == TEXT) {
        if (c->text == entry) {
						struct container *temp = c->next;
            free(c);
            return temp;
        }
        else {
            while (c->next != NULL && c->next->text != entry) {
                if (c->next->next == NULL) {
                    break;
                }
                c = c->next;
            }
            if (c->next != NULL && c->next->text == entry) {
								struct container *temp = c->next;
                c->next = c->next->next;
								free(temp);
            }
            return first;
        }
    }


    return NULL;
}
