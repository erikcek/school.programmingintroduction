#include "game.h"
#include "world.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void print_go_error() {
    printf("Týmto smerom nevedie žiadna cesta.\n");
}

void go_to(struct game *game, struct command *command);
void help_commands(struct game *game, struct command *command);
void look_around(struct game *game, struct command *command);
void use_item(struct game *game, struct command *command);
void take_item(struct game *game, struct command *command);
void put_item(struct game *game, struct command *command);
void about(struct game *game, struct command *command);
void inventar(struct game *game, struct command *command);
void restart(struct game *game, struct command *command);
void end(struct game *game, struct command *command);

struct game* create_game() {
    struct backpack *backpack = create_backpack(10);
    struct container *world = create_world();
    struct parser *parser = create_parser();
    if (backpack == NULL || world == NULL || parser == NULL) {
        destroy_backpack(backpack);
        destroy_world(world);
        destroy_parser(parser);
        return NULL;
    }
    struct game *new_game = calloc(1, sizeof(struct game));
    new_game->backpack = backpack;
    new_game->world = world;
    new_game->parser = parser;
    new_game->current_room = world->room;
    new_game->state = PLAYING;
    return new_game;
}

struct game* destroy_game(struct game* game) {
    if (game == NULL) {
        return NULL;
    }
    destroy_parser(game->parser);
    destroy_world(game->world);
    destroy_backpack(game->backpack);
    free(game);
    return NULL;
}

void play_game(struct game* game) {
    if (game != NULL) {
        while (game->state == PLAYING) {
            char buffer[INPUT_BUFFER_SIZE];
            fgets (buffer, INPUT_BUFFER_SIZE, stdin);
            struct command *c = parse_input(game->parser, buffer);
            execute_command(game, c);
        }
        if (game->state == RESTART) {
            struct game *new_game = create_game();
            destroy_game(game);
            play_game(new_game);
        }
        else {
            destroy_game(game);
        }
    }
}

void execute_command(struct game* game, struct command* command) {
    if (game != NULL && command != NULL) {
//        printf("%s\n", command->name);
        help_commands(game, command);
        go_to(game, command);
        look_around(game, command);
        take_item(game, command);
        use_item(game, command);
        about(game, command);
        inventar(game, command);
        put_item(game, command);
        restart(game, command);
        end(game, command);
    }
}

void end(struct game *game, struct command *command) {
    if (strcmp(command->name, "KONIEC") == 0 || strcmp(command->name, "QUIT") == 0 || strcmp(command->name, "EXIT") == 0 ) {
        game->state = GAMEOVER;
    }
}


void restart(struct game *game, struct command *command) {
    if (strcmp(command->name, "RESTART") == 0 ) {
        game->state = RESTART;
    }
}

void inventar(struct game *game, struct command *command) {
    if (strcmp(command->name, "INVENTAR") == 0 ) {
        struct container *c = game->backpack->items;
        if (c == NULL) {
            printf("Nemas pri sebe nic.\n");
        }
        else {
            printf("Mas pri sebe:\n");
            while (c != NULL) {
                printf("%s: %s\n", c->item->name, c->item->description);
                c = c->next;
            }
        }
    }
}

void about(struct game *game, struct command *command) {
    if (strcmp(command->name, "ABOUT") == 0 || strcmp(command->name, "O HRE") == 0 ) {
        printf("Hrajes napinavu hru, kde je tvojou ulohou vyslobodiť uneseneho kamarata z domu.\nTento dom je stary a nebude to jednoduché.\n");
    }

}

void put_item(struct game *game, struct command *command) {
    if (strcmp(command->name, "POLOZ") == 0) {
        if (command->nmatch >= 3) {
            if (command->groups[2] != NULL && strcmp(command->groups[2], "") != 0) {
                struct item *c = get_item_from_backpack(game->backpack, command->groups[2]);
                if (c == NULL) {
                    printf("Nič také nemáš pri sebe.\n");
                }
                else {
                    delete_item_from_backpack(game->backpack, c);
                    add_item_to_room(game->current_room, c);
                }
            }
            else {
                printf("Neviem čo mám polozit.\n");
            }
        }
        
    }
}


void take_item(struct game *game, struct command *command) {
    if (strcmp(command->name, "VEZMI") == 0) {
        if (command->nmatch >= 3) {
            if (command->groups[2] != NULL && strcmp(command->groups[2], "") != 0) {
                struct item *c = get_from_container_by_name(game->current_room->items, command->groups[2]);
                if (c == NULL) {
                    printf("Nič také tu nie je.\n");
                }
                else {
                    if (c != NULL && c->properties % 2 != 0) {
                        game->current_room->items = remove_container(game->current_room->items, c);
                        add_item_to_backpack(game->backpack, c);
                    }
                    else {
                        printf("Toto nemôžeš zobrať do batohu.\n");
                    }
                }
            }
            else {
                printf("Neviem čo mám zobrať.\n");
            }
        }

    }
}

void use_item(struct game *game, struct command *command) {
    if (strcmp(command->name, "POUZI") == 0) {
        if (command->nmatch >= 3) {
            if (command->groups[2] != NULL) {
                
            }
        }
    }
}

void look_around(struct game *game, struct command *command) {
    if (strcmp(command->name, "ROZHLIADNI SA") == 0) {
        show_room(game->current_room);
    }
}

void help_commands(struct game *game, struct command *command) {
    if (strcmp(command->name, "HELP") == 0 || strcmp(command->name, "POMOC") == 0 || strcmp(command->name, "PRIKAZY") == 0) {
        struct container *c = game->parser->commands;
        while (c != NULL) {
            printf("%s - %s\n", c->command->name, c->command->description);
            c = c->next;
        }
    }
}

void go_to(struct game *game, struct command *command) {

    if (strcmp(command->name, "SEVER") == 0) {
        if (game->current_room->north != NULL) {
            game->current_room = game->current_room->north;
            printf("Prišiel si do miestosti s názvom: %s\n", game->current_room->name);
        }
        else {
            print_go_error();
        }
    }
    if (strcmp(command->name, "JUH") == 0) {
        if (game->current_room->south != NULL) {
            game->current_room = game->current_room->south;
            printf("Prišiel si do miestosti s názvom: %s\n", game->current_room->name);
        }
        else {
            print_go_error();
        }

    }
    if (strcmp(command->name, "ZAPAD") == 0) {
        if (game->current_room->west != NULL) {
            game->current_room = game->current_room->west;
            printf("Prišiel si do miestosti s názvom: %s\n", game->current_room->name);
        }
        else {
            print_go_error();
        }

    }
    if (strcmp(command->name, "VYCHOD") == 0) {
        if (game->current_room->east != NULL) {
            if (strcmp(game->current_room->east->name, "guest room") == 0) {
                if (get_item_from_backpack(game->backpack, "stary kluc") == NULL) {
                    printf("Tieto dver rú zamknuté");
                }
                else {
                    game->current_room = game->current_room->east;
                    printf("Prišiel si do miestosti s názvom: %s\n", game->current_room->name);
                }
            }
            else {
                game->current_room = game->current_room->east;
                printf("Prišiel si do miestosti s názvom: %s\n", game->current_room->name);
            }
        }
        else {
            print_go_error();
        }

    }
}
