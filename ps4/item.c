#include "item.h"
#include <stdlib.h>
#include <string.h>

struct item* create_item(char* name, char* description, unsigned int properties) {
    if (name == NULL || description == NULL) {
        return NULL;
    }
    if (strcmp(name, "") == 0 || strcmp(description, "") == 0) {
        return NULL;
    }
    char *new_name = calloc(strlen(name) + 1, sizeof(char));
    char *new_description = calloc(strlen(description) + 1, sizeof(char));
    strcpy(new_name, name);
    strcpy(new_description, description);
    struct item *ni = calloc(1, sizeof(struct item));
    ni->name = new_name;
    ni->description = new_description;
    ni->properties = properties;
    
    return ni;
}

struct item* destroy_item(struct item* item) {
    if (item != NULL) {
        free(item->name);
        free(item->description);
        free(item);
    }
    return NULL;
}
