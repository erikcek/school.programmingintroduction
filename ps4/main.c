#include <stdio.h>
#include <stdlib.h>
#include "room.h"
#include "item.h"
#include "backpack.h"
#include "command.h"
#include "container.h"
#include "parser.h"
#include "world.h"
#include "game.h"

int main(int argc, const char * argv[]) {
    create_room(NULL, NULL);
    destroy_room(NULL);
    add_item_to_room(NULL, NULL);
    get_item_from_room(NULL, "a");
    set_exits_from_room(NULL, NULL, NULL, NULL, NULL);
    show_room(NULL);
    create_item(NULL, NULL, NONE);
    add_item_to_room(NULL, NULL);
    delete_item_from_room(NULL, NULL);
    struct backpack* backpack = create_backpack(5);
    free(backpack);
    add_item_to_backpack(NULL, NULL);
    get_item_from_backpack(NULL, "zlat");
    delete_item_from_backpack(NULL, NULL);
    struct parser *p = create_parser();
    destroy_parser(p);
    parse_input(NULL, NULL);
    create_container(NULL, ITEM, NULL);
    destroy_containers(NULL);
    get_from_container_by_name(NULL, "a");
    remove_container(NULL, NULL);
    create_command(NULL, NULL, "", (size_t)0);
    destroy_item(NULL);
    struct container *c = create_world();
    destroy_world(c);
    add_room_to_world(NULL, NULL);
    get_room(NULL, NULL);
    destroy_backpack(NULL);
    destroy_command(NULL);
		struct game *g = create_game();
    play_game(g);
 //   destroy_game(g);
    return 0;
}
