#include <stdlib.h>
#include <string.h>
#include "parser.h"
#include "command.h"


struct parser* create_parser() {
    struct parser *new = calloc(1, sizeof(struct parser));
    struct command *end1 = create_command("KONIEC", "Koniec hry", "KONIEC", 1);
    struct command *end2 = create_command("QUIT", "Koniec hry", "QUIT", 1);
    struct command *end3 = create_command("EXIT", "Koniec hry", "EXIT", 1);
    struct command *version = create_command("VERZIA", "Vypise verziu", "VERZIA", 1);
    struct command *north1 = create_command("SEVER", "Presun do miestnosti na sever od aktualnej.", "SEVER", 1);
    struct command *south1 = create_command("JUH", "Presun do miestnosti na juh od aktualnej.", "JUH", 1);
    struct command *west1 = create_command("ZAPAD", "Presun do miestnosti na zapad od aktualnej.", "ZAPAD", 1);
    struct command *east1 = create_command("VYCHOD", "Presun do miestnosti na vychod od aktualnej.", "VYCHOD", 1);
    struct command *look = create_command("ROZHLIADNI SA", "Zozhliadne sa.", "ROZHLIADNI SA", 1);
    struct command *help1 = create_command("PRIKAZY", "Vypise pomoc", "PRIKAZY", 1);
    struct command *help2 = create_command("HELP", "Vypise pomoc", "HELP", 1);
    struct command *help3 = create_command("POMOC", "Vypise pomoc", "POMOC", 1);
    struct command *restart = create_command("RESTART", "Restart", "RESTART", 1);
    struct command *about1 = create_command("ABOUT", "O hre", "ABOUT", 1);
    struct command *about2 = create_command("O HRE", "O hre", "O HRE", 1);
    struct command *take = create_command("VEZMI", "Vezmi", "(VEZMI) ?([0-9a-zA-Z]*)", (size_t)3);
    struct command *put = create_command("POLOZ", "Poloz", "(POLOZ) ?([0-9a-zA-Z]*)", (size_t)3);
    struct command *inventar1 = create_command("INVENTAR", "Inventar", "INVENTAR", 1);
    struct command *use = create_command("POUZI", "Pouzi", "(POUZI) ([0-9a-zA-Z]*)", (size_t)3);
    struct command *investigate = create_command("PRESKUMAJ", "Preskumaj", "PRESKUMAJ", 1);
    struct command *save1 = create_command("ULOZ", "save", "ULOZ", 1);
    struct command *save2 = create_command("SAVE", "save", "SAVE", 1);
    struct command *load1 = create_command("NAHRAJ", "load", "NAHRAJ", 1);
    struct command *load2 = create_command("LOAD", "load", "LOAD", 1);
    struct command *north2 = create_command("SEVER", "Presun do miestnosti na sever od aktualnej.", "s\n", 1);
    //    struct command *north3 = create_command("SEVER", "Presun do miestnosti na sever od aktualnej.", "^S", 1);
    //    struct command *north4 = create_command("SEVER", "Presun do miestnosti na sever od aktualnej.", " S ", 1);
    struct command *south2 = create_command("JUH", "Presun do miestnosti na juh od aktualnej.", "j\n", 1);
    //    struct command *south3 = create_command("JUH", "Presun do miestnosti na juh od aktualnej.", "^j", 1);
    //    struct command *south4 = create_command("JUH", "Presun do miestnosti na juh od aktualnej.", " j ", 1);
    struct command *west2 = create_command("ZAPAD", "Presun do miestnosti na zapad od aktualnej.", "Z\n", 1);
    //    struct command *west3 = create_command("ZAPAD", "Presun do miestnosti na zapad od aktualnej.", "^Z", 1);
    //    struct command *west4 = create_command("ZAPAD", "Presun do miestnosti na zapad od aktualnej.", " Z ", 1);
    struct command *east2 = create_command("VYCHOD", "Presun do miestnosti na vychod od aktualnej.", "V\n", 1);
    //    struct command *east3 = create_command("VYCHOD", "Presun do miestnosti na vychod od aktualnej.", "^V", 1);
    //    struct command *east4 = create_command("VYCHOD", "Presun do miestnosti na vychod od aktualnej.", " V ", 1);
    struct command *inventar2 = create_command("INVENTAR", "Inventar", "i\n", 1);
    //    struct command *inventar3 = create_command("I", "Inventar", "^I", 1);
    struct command *inventar4 = create_command("INVENTAR", "Inventar", " I ", 1);
    struct container *c = create_container(NULL, COMMAND, end1);
    create_container(c, COMMAND, end2);
    create_container(c, COMMAND, end3);
    create_container(c, COMMAND, take);
    create_container(c, COMMAND, version);
    create_container(c, COMMAND, north1);
    create_container(c, COMMAND, north2);
    //    create_container(c, COMMAND, north3);
    //    create_container(c, COMMAND, north4);
    create_container(c, COMMAND, south1);
    create_container(c, COMMAND, south2);
    //    create_container(c, COMMAND, south3);
    //    create_container(c, COMMAND, south4);
    create_container(c, COMMAND, west1);
    create_container(c, COMMAND, west2);
    //    create_container(c, COMMAND, west3);
    //    create_container(c, COMMAND, west4);
    create_container(c, COMMAND, east1);
    create_container(c, COMMAND, east2);
    //    create_container(c, COMMAND, east3);
    //    create_container(c, COMMAND, east4);
    create_container(c, COMMAND, look);
    create_container(c, COMMAND, help1);
    create_container(c, COMMAND, help2);
    create_container(c, COMMAND, help3);
    create_container(c, COMMAND, restart);
    create_container(c, COMMAND, about1);
    create_container(c, COMMAND, about2);
    create_container(c, COMMAND, put);
    create_container(c, COMMAND, inventar1);
    create_container(c, COMMAND, inventar2);
    //    create_container(c, COMMAND, inventar3);
    create_container(c, COMMAND, inventar4);
    create_container(c, COMMAND, use);
    create_container(c, COMMAND, investigate);
    create_container(c, COMMAND, save1);
    create_container(c, COMMAND, save2);
    create_container(c, COMMAND, load1);
    create_container(c, COMMAND, load2);
    new->commands = c;
    return new;
}

struct parser* destroy_parser(struct parser* parser) {
    if (parser == NULL) {
        return NULL;
    }
    destroy_containers(parser->commands);
    destroy_containers(parser->history);
    free(parser);
    return NULL;
}

struct command* parse_input(struct parser* parser, char* input) {
    if (parser == NULL || input == NULL) {
        return NULL;
    }
    if (parser->commands == NULL) {
        return NULL;
    }
    struct container *c = parser->commands;
    do {
        regmatch_t *groups = calloc(c->command->nmatch, sizeof(regmatch_t));
        int result = regexec(&c->command->preg, input, c->command->nmatch, groups, 0);
        if (result != REG_NOMATCH) {
            if (c->command->nmatch > 0) {
                if (c->command->groups != NULL) {
                    for (int i=0; i<c->command->nmatch; i++) {
                        free(c->command->groups[i]);
                    }
                    free(c->command->groups);
                }
                char **new_groups = calloc(c->command->nmatch, sizeof(char *));
                c->command->groups = new_groups;
                for(int i = 0; i < c->command->nmatch; i++){
                    if(groups[i].rm_so == -1){
                        break;
                    }
                    char *new = calloc((size_t)(groups[i].rm_eo - groups[i].rm_so + 2), sizeof(char));
                    strncpy(new, (input + (int)groups[i].rm_so), (size_t)(groups[i].rm_eo - groups[i].rm_so));
                    c->command->groups[i] = new;
                    
                }
            }
            free(groups);
            return c->command;
        }
        free(groups);
        c = c->next;
    } while (c != NULL);
    
    return NULL;
}
