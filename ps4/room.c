#include "room.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct room* create_room(char *name, char *description) {
    if (name == NULL || description == NULL) {
        return NULL;
    }
    if (strcmp(name, "") == 0 || strcmp(description, "") == 0) {
        return NULL;
    }
    char *new_name = calloc(strlen(name) + 1, sizeof(char));
    char *new_description = calloc(strlen(description) + 1, sizeof(char));
    strcpy(new_name, name);
    strcpy(new_description, description);
    struct room *nr = calloc(1, sizeof(struct room));
    nr->name = new_name;
    nr->description =  new_description;
    nr->north = NULL;
    nr->south = NULL;
    nr->east = NULL;
    nr->west = NULL;
    nr->items = NULL;
    return nr;
}

void set_exits_from_room(struct room *room, struct room *north, struct room *south, struct room *east, struct room *west) {
    if (room != NULL) {
        room->east = east;
        room->west = west;
        room->north = north;
        room->south = south;
    }
}

void show_room(const struct room* room) {
    if (room != NULL) {
        printf("Nachádzaš sa v miestosti s názvom: %s\n", room->name);
        if (room->items != NULL) {
            struct container *c = room->items;
            while(c != NULL) {
                printf("%s - %s\n", c->item->name, c->item->description);
                c = c->next;
            }
        }
    }
}

struct room* destroy_room(struct room* room) {
    if (room == NULL) {
        return NULL;
    }
    free(room->name);
    free(room->description);
    if (room->items) {
        destroy_containers(room->items);
    }
    free(room);
    return NULL;
}

void add_item_to_room(struct room* room, struct item* item) {
    if (room != NULL) {
        if (room->items == NULL) {
            room->items = create_container(NULL, ITEM, item);
        }
        else {
            create_container(room->items, ITEM, item);
        }
    }
}

void delete_item_from_room(struct room* room, struct item* item) {
    if (room != NULL && room->items != NULL) {
        struct container *ci = room->items;
        room->items = remove_container(ci, item);
    }
}

struct item* get_item_from_room(const struct room* room, const char* name) {
    if (room == NULL) {
        return NULL;
    }
    if (room->items != NULL) {
        struct container *ci = room->items;
        struct item *i = get_from_container_by_name(ci, name);
        return i;
    }
    return NULL;
}

