#include "world.h"
#include <stdlib.h>

struct container* create_world() {
    struct room *r1 = create_room("entry", "entry to house");
    struct room *r2 = create_room("big library", "big library");
    struct room *r3 = create_room("small library", "small library");
    struct room *r4 = create_room("armory", "armory");
    struct room *r5 = create_room("hall", "hall");
    struct room *r6 = create_room("basement", "basement");
    struct room *r7 = create_room("large hallawy", "large hallaway");
    struct room *r8 = create_room("small bedroom", "small bedroom");
    struct room *r9 = create_room("medium bedroom", "medium bedroom");
    struct room *r10 = create_room("large bedroom", "large bedroom");
    struct room *r11 = create_room("entry hallaway", "entry hallaway");
    struct room *r12 = create_room("basement hallaway", "basement hallaway");
    struct room *r13 = create_room("guest room", "guest room");
    struct room *r14 = create_room("garage", "garage");
    struct room *r15 = create_room("forest", "forest");

    set_exits_from_room(r1, NULL, NULL, r11, NULL);
    
    set_exits_from_room(r11, r3, r14, r5, r1);
    set_exits_from_room(r3, r2, r11, NULL, NULL);
    set_exits_from_room(r2, NULL, r3, NULL, NULL);
    
    set_exits_from_room(r14, r11, r15, r6, NULL);
    set_exits_from_room(r15, r14, NULL, NULL, NULL);
    
    set_exits_from_room(r6, NULL, NULL, r12, r14);
    set_exits_from_room(r12, NULL, NULL, r13, r6);
    set_exits_from_room(r13, NULL, NULL, NULL, r12);
    
    set_exits_from_room(r5, NULL, NULL, r7, r11);
    set_exits_from_room(r7, r8, NULL, r4, r5);
    set_exits_from_room(r4, NULL, NULL, NULL, r7);
    set_exits_from_room(r8, r9, r7, NULL, NULL);
    set_exits_from_room(r9, r10, r8, NULL, NULL);
    set_exits_from_room(r10, NULL, r9, NULL, NULL);

    struct item *i1 = create_item("kluc", "velmi stary kluc, skus najst stare dvere", MOVABLE);
    struct item *i2 = create_item("mec", "dlhy mec, bud opatrny", MOVABLE);
    struct item *i3 = create_item("noz", "dobre vyzerajúci noz", MOVABLE);
    struct item *i4 = create_item("sekera", "sekera v dobrom stave", MOVABLE);
    struct item *i5 = create_item("listok", "listok na ktorom je napásané 'guest room'", MOVABLE);
    struct item *i6 = create_item("krabicka", "zvlasta krabiska", MOVABLE);

    add_item_to_room(r15, i4);
    add_item_to_room(r2, i1);
    add_item_to_room(r4, i3);
    add_item_to_room(r12, i2);
    add_item_to_room(r3, i5);
    add_item_to_room(r3, i6);
    
    struct container *c = create_container(NULL, ROOM, r1);
    create_container(c, ROOM, r2);
    create_container(c, ROOM, r3);
    create_container(c, ROOM, r4);
    create_container(c, ROOM, r5);
    create_container(c, ROOM, r6);
    create_container(c, ROOM, r7);
    create_container(c, ROOM, r8);
    create_container(c, ROOM, r9);
    create_container(c, ROOM, r10);
    create_container(c, ROOM, r11);
    create_container(c, ROOM, r12);
    create_container(c, ROOM, r13);
    create_container(c, ROOM, r14);
    create_container(c, ROOM, r15);
//    return NULL;
    return c;
}

struct container* add_room_to_world(struct container* world, struct room* room) {
    if (room == NULL && world == NULL) {
        return NULL;
    }
    if (world == NULL) {
        return create_container(NULL, ROOM, room);
    }
    if (room == NULL) {
        return NULL;
    }
    if (get_from_container_by_name(world, room->name) == NULL) {
        return create_container(world, ROOM, room);
    }
    else {
        return NULL;
    }
}

struct container* destroy_world(struct container* world) {
    if (world == NULL) {
        return NULL;
    }
//    struct container *c = world;
//    while(c != NULL) {
//        if (c->room != NULL && c->room->items != NULL) {
//            //destroy_containers(c->room->items);
//        }
//        c = c->next;
//    }
    destroy_containers(world);
    return NULL;
}

struct room* get_room(struct container* world, char* name) {
    if (world == NULL) {
        return NULL;
    }
    return get_from_container_by_name(world, name);
}
