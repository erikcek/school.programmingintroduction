#include "mastermind.h"
#include <stdlib.h>
#include <Arduino.h>

int getLedNumber(int index, bool red) {
	if (red) {
		switch(index) {
			case 0:
				return LED_RED_1;
			case 1:
				 return LED_RED_2;
			case 2:
				return LED_RED_3;
			case 3:
				return LED_RED_4;
		}
	}
	else {
		switch(index) {
			case 0:
				return LED_BLUE_1;
			case 1:
				 return LED_BLUE_2;
			case 2:
				return LED_BLUE_3;
			case 3:
				return LED_BLUE_4;
		}
	}
}

char* generate_code(bool repeat, int length) {
	char digits[] = "0123456789";
 	if (length <= 0) {
    	return NULL; 
  	}
	char *c = (char*)calloc(length + 1, sizeof(char));
	for (int i=0; i<length; i++) {
		if (repeat) {
			*(c + i) = digits[random(10)];
		}
		else {
			if (length > 10) {
	   			return NULL;
	  		}
		  	bool isAllreadyIn = false;
		  	char temp;
		  	do {
		  		isAllreadyIn = false;
			    temp = digits[random(10)];
			    for (int j = 0; j<i; j++) {
				      if (*(c + j) == temp) {
				      		isAllreadyIn = true;
				      }
			    }
		  	} while (isAllreadyIn == true);
			*(c + i) = temp;	
		}
	} 
	*(c + length) = '\0';
	return c;
}

void render_leds(const int peg_a, const int peg_b) {
	int offset = 0;
	for (int i=0; i<peg_a; i++) {
		int led = getLedNumber(i, true);
		digitalWrite(led, HIGH);
		offset++;
	}
	for (int i=0; i<peg_b; i++) {
		int led = getLedNumber(i + offset, false);
		digitalWrite(led, HIGH);
	}
}

void turn_off_leds() {
	for (int i=0; i<4; i++) {
		int red = getLedNumber(i, true);
		int blue = getLedNumber(i, false);
		digitalWrite(red, LOW);
		digitalWrite(blue, LOW);
	}
}

void get_score(const char* secret, const char* guess, int* peg_a, int* peg_b) {
	int secret_length = strlen(secret);
	int guess_length = strlen(guess);
	if (secret_length != guess_length) {
		return;	
	}
	
	*peg_a = 0;
	for (int i=0; i<secret_length; i++) {
		if (*(secret + i) == *(guess + i)) {
			*peg_a += 1;
		}
	}
	*peg_b = 0;
	for (int i=0; i<secret_length; i++) {
		for (int j=0; j<guess_length; j++) {
			if (*(secret + i) == *(guess + j)) {
				*peg_b +=1;
				break;
			}
		}
	}
}

void render_history(char* secret, char** history, const int entry_nr) {
	int a = 0;
	int b = 0;
	get_score(secret, history[entry_nr], &a, &b);
	render_leds(a,b);
}







	
