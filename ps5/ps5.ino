#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <stdlib.h>
#include "lcd_wrapper.h"
#include "mastermind.h"


void setup()
{
	lcd_init(); //initialize the lcd
	Serial.begin(9600);
	for (int i=6; i< 14; i++) {
		pinMode(i, OUTPUT);
	}
}


void loop()
{
	char *c = generate_code(false, 4);
	lcd_print_at(0,0, c);
	free(c);   
	int a = 0;
	int b = 0;
	get_score("1234", "1423", &a, &b);
	Serial.println("##");
	Serial.println(a);
	Serial.println(b);
	render_leds(a, b);
	delay(1000);
	turn_off_leds();
	delay(1000);
}
